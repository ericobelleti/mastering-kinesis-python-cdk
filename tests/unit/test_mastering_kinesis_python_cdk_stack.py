import aws_cdk as core
import aws_cdk.assertions as assertions

from mastering_kinesis_python_cdk.mastering_kinesis_python_cdk_stack import MasteringKinesisPythonCdkStack

# example tests. To run these tests, uncomment this file along with the example
# resource in mastering_kinesis_python_cdk/mastering_kinesis_python_cdk_stack.py
def test_sqs_queue_created():
    app = core.App()
    stack = MasteringKinesisPythonCdkStack(app, "mastering-kinesis-python-cdk")
    template = assertions.Template.from_stack(stack)

#     template.has_resource_properties("AWS::SQS::Queue", {
#         "VisibilityTimeout": 300
#     })
