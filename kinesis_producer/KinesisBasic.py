import json
import logging
import boto3
from botocore.exceptions import ClientError
logger = logging.getLogger(__name__)
stream_name = "testBox"
client = boto3.client('kinesis')


def put_kinesis_record(data, partition_key):
    try:
        response = client.put_record(
        StreamName= stream_name,
        Data=json.dumps(data),
        PartitionKey=partition_key)
        logger.info("Put record in stream %s.", stream_name)
    except ClientError:
        logger.exception("Couldn't put record in stream %s.", stream_name)

        raise
    else:
        return response



def send_seperate_records(partition_keys):
    for key in partition_keys:
        put_kinesis_record({
            "importantStuff":"your data is important",
            "smthElse":"Kinesis will take care of it"
        })
