from KinesisBasic import send_seperate_records


def generate_keys(count):
    n = []
    for i in range(count):
        n.append(str(i))
    return n



def handler(event, context):
    if(event['recordCount']):
        print("Event is missing required parameters")
    partition_keys =generate_keys(event['recordCount'])


    send_seperate_records(partition_keys)

