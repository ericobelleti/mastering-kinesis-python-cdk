from aws_cdk import Stack
import aws_cdk.aws_lambda as lambda_
import aws_cdk.aws_kinesis as kinesis
import aws_cdk
from constructs import Construct

#https://constructs.dev/packages/@aws-cdk/aws-lambda/v/1.178.0?lang=python

class MasteringKinesisPythonCdkStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        stream = kinesis.Stream(self,
            id = "kinesisstream",
            stream_name="ericstream",
            shard_count=1,
            stream_mode=kinesis.StreamMode.PROVISIONED,
           encryption=kinesis.StreamEncryption.UNENCRYPTED,
                                )


        producer  = lambda_.Function(self,
           id="lamdafunc1",
           function_name="KinesisProducer",
           runtime=lambda_.Runtime.PYTHON_3_9,
           code=lambda_.Code.from_asset(path="kinesis_producer"),
           handler='producer.handler',
           timeout= aws_cdk.Duration.minutes(5),
           memory_size=1024,
           environment= {
               "STREAM_NAME":stream.stream_name,
               "MAX_RETRIES":'10'
           }
       )
        stream.grant_write(producer)
        consumer = lambda_.Function(self,
                                    id ="lambdafunc2",
                                    function_name="KinesisConsumer",
                                    runtime=lambda_.Runtime.PYTHON_3_9,
                                    code=lambda_.Code.from_asset(path="kinesis_consumer"),
                                    handler='consumer.handler',

                                    environment={

                                    }
                                    )

        stream.grant_read(consumer)


